const { Router } = require('express')

const router = Router()

router.get('/', (req, res) => {
  res.json({
    msg: 'FUnciono desde api',
  })
})

router.get('/funciones', (req, res) => {
  res.json({
    msg: 'FUnciono desde api para el usuario',
  })
})

router.get('/profesiones', (req, res) => {
  res.json({
    msg: 'FUnciono desde api para el usuario y profesiones',
  })
})

module.exports = router
